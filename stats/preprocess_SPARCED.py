import pandas as pd
import numpy as np

def preprocess_sparced(base_dir, mtx, time, species):
    '''
    Provide file names and this will read in the SPARCED data
    Typically:
        base_dir = <YOUR DATA LOCATION>
        mtx = 'xoutS_deterministic.txt' OR 'xoutS_stochastic.txt'
        time = 'tout_all.txt'
        species = 'Species.txt'
    Returns a Pandas df
    '''
    ### Reading in the column headers
    species_df = pd.read_csv(base_dir + species, sep='\t')
    species_col_headers = list(species_df['species'])

    ### Reading in the index labels
    tout_all = pd.read_csv(base_dir + time, sep='\t', header=None)
    time_indices = list(tout_all[0])

    ### Reading in the actual files
    mtx_df = pd.read_csv(base + mtx, sep='\t', names=species_col_headers, index_col=False)

    # Set the indices to be time
    mtx_df.index=time_indices

    return mtx_df
