import pandas as pd
from statsmodels.tsa.stattools import grangercausalitytests
from statsmodels.tsa.stattools import adfuller
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from scipy import signal
import numpy as np
import random
import math
np.seterr(divide='ignore')


def random_sample_cols(df, n_cols, seed=9):
    df = df.sample(n=n_cols,axis='columns', random_state=seed)
    return df

def adf_test(timeseries, print_p=False):
    '''
    Performs Augmented Dickey Fuller Test to estimate whether a time-series is static
    Null Hypothesis --> the series in question varies as a function of time
    If p-value < 0.05, then the series is stationary
    '''
    
    dftest = adfuller(timeseries, autolag='AIC')
    test_columns = ['Test Statistic','P-value','Lags Used','No of Observations']
    result = pd.Series(dftest[0:4], index=test_columns)
    for key,value in dftest[4].items():
        result['Critical Value (%s)'%key] = value
        
    if print_p == True:
        p_value = result['P-value']
        p_value = round(p_value, 4)
        print(p_value)
        print('------')
        if p_value < 0.05:
            print('< 0.05, so reject the null hypothesis')
            print('The series appears stationary.')
        else:
            print('>= 0.05, so fail to reject the null hypothesis')
            print('The series is variable as a function of time')
        print('------')
        
    return result
    
def adf_test_mtx(time_df):
    adf_results = time_df.apply(adf_test, axis = 0)

    return adf_results

def select_variable_ts(time_df, adf_results, p_value=0.05):
    variable_species = adf_results.T[adf_results.T['P-value'] >= p_value].index
    variable_df = time_df[variable_species]
    
    n_all = len(time_df.columns)
    n_var = len(variable_df.columns)
    percent_var = round((n_var / n_all), 4) * 100
    print('------')
    print('{} % of the series were found to be variable as a function of time'.format(percent_var))
    print('{} / {}'.format(n_var, n_all))
    print('------')
    
    return variable_df

def visualize_series(df, col_to_plot):
    '''
    This function creates a scatter plot of time-series data.
        df --> the pandas dataframe containing the series of interest.
        NOTE: the row indices should be the time series, columns are species
        cols_to_plot --> list of the columns within df to visualize
    '''
    
    fig = plt.figure(figsize=(9,3))
    ax = plt.axes()
    plt.margins(x=0)
    ax.set_facecolor("whitesmoke")
    ax.set_axisbelow(True)
    plt.grid(color = 'white', linestyle = '-', linewidth = 1)
    plt.title(col_to_plot, fontsize=15)
    ax.set_xlabel("Time", size=14)
    ax.set_ylabel("Concentration", size=14)

    x_scatter = df.index
    y_scatter = df[col_to_plot]
    ax.plot(x_scatter, y_scatter, '-', color='royalblue')

    for label in ax.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)

    plt.setp(ax.xaxis.get_majorticklabels(), rotation=45, ha="right" )
    plt.show()

def granger_test(df, chicken, egg, maxlag=5):
    '''
	This is testing whether the series of eggs is predictive of future chicken quantities.
    '''
    test = grangercausalitytests(df[[chicken, egg]], maxlag=[maxlag])
    return test

def ccf_lags(series1, series2):
    p = series1
    q = series2
    p = (p - np.mean(p)) / (np.std(p) * len(p))
    q = (q - np.mean(q)) / (np.std(q))  
    c = np.correlate(p, q, 'full')
    return c

def ccf_correlation(series1, series2):
    lags = signal.correlation_lags(len(series1), len(series2))
    return lags

def ccf_plot(lags, ccf, series1, series2):
    fig, ax =plt.subplots(figsize=(9, 4))
    ax = plt.axes()
    plt.margins(x=0)
    ax.set_facecolor("whitesmoke")
    ax.set_axisbelow(True)
    plt.grid(color = 'white', linestyle = '-', linewidth = 1)
    ax.plot(lags, ccf, '-', color='royalblue')
    ax.axhline(-2/np.sqrt(23), color='red', label='5% confidence interval')
    ax.axhline(2/np.sqrt(23), color='red')
    ax.axvline(x = 0, color = 'black', lw = 1)
    ax.axhline(y = 0, color = 'black', lw = 1)
    ax.axhline(y = np.max(ccf), color = 'blue', lw = 1, linestyle='--', label = 'highest +/- correlation')
    ax.axhline(y = np.min(ccf), color = 'blue', lw = 1, linestyle='--')
    ax.set(ylim = [-1, 1])
    ax.set_title('Cross Correation between {} and {}'.format(series1.name, series2.name), weight='bold', fontsize = 15)
    ax.set_ylabel('Correlation Coefficients', weight='bold', fontsize = 12)
    ax.set_xlabel('Time Lags', weight='bold', fontsize = 12)
    plt.setp(ax.xaxis.get_majorticklabels(), rotation=45, ha="right" )
    plt.legend()
