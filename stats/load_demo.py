import pandas as pd

def load_SPARCED(base_dir="/app/"):
    '''
    Load the Birtwistle deterministic SPARCED model
    base_dir should be the absoslute path for the `timeseries-sandbox` clone
    '''
    fin = base_dir + 'demo_data/SPARCED_deterministic.csv'
    sparced_demo = pd.read_csv(fin, sep=',')
    sparced_demo.rename(columns={'Unnamed: 0':'time'}, inplace=True)
    sparced_demo.set_index('time', inplace=True)
    return sparced_demo

def load_Tabby():
    '''
    Load the data from Tabby's star
    '''
    fin = '/app/demo_data/Tabbys_Data.csv'
    tabbys_demo = pd.read_csv(fin)
    return tabbys_demo
