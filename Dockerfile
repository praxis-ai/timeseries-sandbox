

FROM jupyter/scipy-notebook

RUN DEFAULT_USER=$USER
USER root

RUN mkdir /app
WORKDIR /app

COPY . /app

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
	git tmux nano \
	pip \
        && apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

RUN pip3 install -r /app/requirements.txt

ENV PYTHONPATH=$PYTHONPATH:/app/

USER $DEFAULT_USER
