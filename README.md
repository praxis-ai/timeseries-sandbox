# Time Series Analysis Sandbox

For further stats analyses, see:
https://towardsdatascience.com/a-quick-introduction-on-granger-causality-testing-for-time-series-analysis-7113dc9420d2

## Running on your local machine...
#### Dependencies:
* Git
* Docker

#### Process
Clone the repo...
```
$ git clone https://gitlab.com/praxis-ai/timeseries-sandbox.git
# Login to Gitlab

$ cd timeseries-sandbox
```

Now we can launch the Docker container from within this 
```
$ # We can pull the docker image and run it locally
$ docker pull registry.gitlab.com/praxis-ai/timeseries-sandbox:2.1
$ docker run -p 8888:8888 -t registry.gitlab.com/praxis-ai/timeseries-sandbox:2.1 | grep 'http://127.0.0.1:8888/lab\?'
```
NOTE: If you'd like to copy any data from your local machine into the instance (example data will already be there), then that can be done with:
`$ docker run -p 8888:8888 -v /dir/on/host/data:/app -t -d registry.gitlab.com/praxis-ai/timeseries-sandbox:2.1`

You should see an output printed to the consle that contains a series of IP addresses.
Find the one beginning with `http://127.0.0.1:8888/lab?token=*` and copy it into your browser.

From there, navigate to the `notebooks` directory and find `sandbox_Demo.ipynb`. More detailed instructions about the statistical analysis can \
be explored there.

## Running on the Palmetto Cluster...
#### Dependencies
* git
* conda
* pip

#### Process
To run on Palmetto, you'll need to utilize anaconda environments rather than Docker containers to utilize these tools.

Clone the repo...
```
$ git clone https://gitlab.com/praxis-ai/timeseries-sandbox.git
# Login to Gitlab

$ cd /timeseries-sandbox
```

`ssh` into a resource that you have access to (not the login node!) and create a new conda environment. 
```
# PYTHONPATH=$PYTHONPATH:<PATH_TO_timeseries-sandbox>
conda create -n timeseries_env python=3.10.6
conda activate timeseries_env

# Install all requirements
pip install -r requirements.txt
pip install ipykernel

# Make this environment available to Jupyter
ipython kernel install --user --name=timeseries_env
```

With all of this set up, now you will need to access a [Jupyterhub Environment on Palmetto](https://www.palmetto.clemson.edu/palmetto/basic/jupyter/).

From there, you can access the `sandbox_demo_.ipynb` and begin playing with these time-series statistical analyses.


